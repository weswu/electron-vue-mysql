module.exports = {
  pluginOptions: {
    electronBuilder: {
      nodeIntegration: true,
      builderOptions: {
        productName: '航大北斗应用', //项目名 这也是生成的exe文件的前缀名
        appId: 'com.wes.www', //包名
        copyright: 'Copyright @ 2022 Wes', //版权信息
        mac: {
          icon: 'src/assets/icon/icon.icns',
          category: 'public.app-category.developer-tools',
          target: 'default',
          extendInfo: {
            LSUIElement: 1
          }
        },
        win: {
          icon: 'src/assets/icons/icon.ico', //图标路径,
          target: 'nsis'
        },
        nsis: {
          oneClick: false, // 一键安装
          include: 'src/assets/nsis/installer.nsi', // 自定义安装界面
          allowToChangeInstallationDirectory: true // 允许修改安装目录
          // "guid": "xxxx", //注册表名字，不推荐修改
          // "perMachine": true, // 是否开启安装时权限限制（此电脑或当前用户）
          // "allowElevation": true, // 允许请求提升。 如果为false，则用户必须使用提升的权限重新启动安装程序。
          // "installerIcon": "./src/assets/icons/aaa.ico", // 安装图标
          // "uninstallerIcon": "./src/assets/icons/bbb.ico", //卸载图标
          // "installerHeaderIcon": "./src/assets/icons/aaa.ico", // 安装时头部图标
          // "createDesktopShortcut": true, // 创建桌面图标
          // "createStartMenuShortcut": true, // 创建开始菜单图标
          // "shortcutName": "xxxx" // 图标名称
        },
        linux: {
          icon: 'src/assets/icons/'
        },
      }
    }
  }
}
