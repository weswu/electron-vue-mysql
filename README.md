# desktop-vue-mysql
>桌面端应用 + Vue 开发 不用后端api接口，直连数据库操作

## Project setup
```
cnpm install -g @vue/cli  or  yarn global add @vue/cli
vue add electron-builder
yarn config set ignore-engines true (安装不成功时)
```

### Compiles and hot-reloads for development
```
yarn install
yarn dev
```

### Compiles and minifies for production
```
yarn electron:build
```

#### 离线打包需下载的文件
- 1.[下载](https://github.com/electron/electron/releases/tag/v13.6.3)
  `electron-v13.6.3-win32-x64.zip` 、`SHASUMS256.txt-13.6.3` 放到 `C:\Users\Administrator\AppData\Local\electron\Cache`

- 2.[下载](https://github.com/electron-userland/electron-builder-binaries/releases/tag/winCodeSign-2.6.0)
  `winCodeSign-2.6.0` 放到 `C:\Users\Administrator\AppData\Local\electron-builder\Cache\winCodeSign`  

- 3.[下载](https://github.com/electron-userland/electron-builder-binaries/releases/tag/nsis-3.0.4.2)
  `nsis-3.0.4.2` 放到 `C:\Users\Administrator\AppData\Local\electron-builder\Cache\nsis`

- 4.[下载](https://github.com/electron-userland/electron-builder-binaries/releases/tag/nsis-resources-3.4.1)
  `nsis-resources-3.4.1` 放到 `C:\Users\Administrator\AppData\Local\electron-builder\Cache\nsis`


### 数据库操作

#### 使用sequelize-auto生成sequelize的Models
```
node_modules/sequelize-auto/bin/sequelize-auto -o './src/dbs/models' -h 192.168.1.244 -d test -u root -x 123456 -p 3306 -t sys_user
```


### 自定义安装界面 NSIS


### 技术支持
- [electron-vue](https://github.com/nklayman/vue-cli-plugin-electron-builder).
- [electron-doc](https://www.electronjs.org/zh/docs/latest/).
