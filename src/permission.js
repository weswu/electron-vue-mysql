/**
 * 全站权限配置
 *
 */
import router from './router/index'

router.beforeEach((to, from, next) => {
  const meta = to.meta || {}
  if(from.path === '/home'){
    next('/login')
  } else {
    next()
  }
})

router.afterEach(() => {
  let title = 'aaaa';
})
