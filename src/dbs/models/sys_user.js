const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sys_user', {
    user_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "主键ID"
    },
    username: {
      type: DataTypes.STRING(64),
      allowNull: true
    },
    password: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    salt: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    phone: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    avatar: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    dept_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "部门ID"
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP'),
      comment: "创建时间"
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: "修改时间"
    },
    lock_flag: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "0"
    },
    del_flag: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "0"
    },
    wx_openid: {
      type: DataTypes.STRING(32),
      allowNull: true,
      comment: "微信登录openId"
    },
    mini_openid: {
      type: DataTypes.STRING(32),
      allowNull: true,
      comment: "小程序openId"
    },
    qq_openid: {
      type: DataTypes.STRING(32),
      allowNull: true,
      comment: "QQ openId"
    },
    gitee_login: {
      type: DataTypes.STRING(100),
      allowNull: true,
      comment: "码云 标识"
    },
    osc_id: {
      type: DataTypes.STRING(100),
      allowNull: true,
      comment: "开源中国 标识"
    },
    tenant_id: {
      type: DataTypes.INTEGER,
      allowNull: false,
      defaultValue: 0,
      comment: "所属租户"
    }
  }, {
    sequelize,
    tableName: 'sys_user',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "user_id" },
        ]
      },
      {
        name: "user_wx_openid",
        using: "BTREE",
        fields: [
          { name: "wx_openid" },
        ]
      },
      {
        name: "user_qq_openid",
        using: "BTREE",
        fields: [
          { name: "qq_openid" },
        ]
      },
      {
        name: "user_idx1_username",
        using: "BTREE",
        fields: [
          { name: "username" },
        ]
      },
    ]
  });
};
