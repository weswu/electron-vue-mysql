import Vue from 'vue'
import App from './App.vue'
import router from './router'
Vue.use(router)
import './permission' // 权限

import ElementUI from 'element-ui'
import './styles/common.scss'

Vue.use(ElementUI, {
  size: 'small',
  menuType: 'text'
})

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')
