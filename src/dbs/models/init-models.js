var DataTypes = require("sequelize").DataTypes;
var _sys_menu = require("./sys_menu");

function initModels(sequelize) {
  var sys_menu = _sys_menu(sequelize, DataTypes);


  return {
    sys_menu,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
