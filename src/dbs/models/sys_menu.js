const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('sys_menu', {
    menu_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true,
      comment: "菜单ID"
    },
    name: {
      type: DataTypes.STRING(32),
      allowNull: true
    },
    permission: {
      type: DataTypes.STRING(32),
      allowNull: true
    },
    path: {
      type: DataTypes.STRING(128),
      allowNull: true
    },
    parent_id: {
      type: DataTypes.INTEGER,
      allowNull: true,
      comment: "父菜单ID"
    },
    icon: {
      type: DataTypes.STRING(32),
      allowNull: true
    },
    sort: {
      type: DataTypes.INTEGER,
      allowNull: true,
      defaultValue: 1,
      comment: "排序值"
    },
    keep_alive: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "0"
    },
    type: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "0"
    },
    create_time: {
      type: DataTypes.DATE,
      allowNull: true,
      defaultValue: Sequelize.Sequelize.literal('CURRENT_TIMESTAMP'),
      comment: "创建时间"
    },
    update_time: {
      type: DataTypes.DATE,
      allowNull: true,
      comment: "更新时间"
    },
    del_flag: {
      type: DataTypes.CHAR(1),
      allowNull: true,
      defaultValue: "0"
    },
    tenant_id: {
      type: DataTypes.INTEGER.UNSIGNED,
      allowNull: true,
      comment: "租户ID"
    }
  }, {
    sequelize,
    tableName: 'sys_menu',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "menu_id" },
        ]
      },
    ]
  });
};
